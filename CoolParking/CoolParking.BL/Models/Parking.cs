﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private Parking() {}
        private static Parking _instance;
        public static Parking GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Parking();
            }

            return _instance;
        }

        public decimal parkBalance { get; set; }
        public List<Vehicle> ParkVehicles = new List<Vehicle>();

    }
}
