﻿using System;
using System.Text;
using Microsoft.VisualBasic;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicalType { get; }
       public decimal Balance { get; set; }

        public Vehicle(string id, VehicleType vehicalType, decimal balance)
        {
            if (id.Length != 10)
            {
                throw new ArgumentException("Invalid ID");
            }
            
            if (id[2] == id[7] && id[7] == '-' && IsUpperValid(id,0,2) &&
                IsUpperValid(id, 8, 2) && IsDigitValid(id,3,4))            
            {
               Id = id; 
            }
            else
            {
                throw new ArgumentException("Invalid ID");
            }

            VehicalType = vehicalType;
            Balance = balance;
        }

        private bool IsUpperValid(string id, int startIndex, int length)
        {
            foreach (char c in id.Substring(startIndex,length))
            {
                if ((int) c < 65 && (int) c > 90)
                {
                    return false;
                }
            }

            return true;
        }

        private bool IsDigitValid(string id, int startIndex, int length)
        {
            foreach (char c in id.Substring(startIndex, length))
            {
                if ((int)c < 48 && (int)c > 57)
                {
                    return false;
                }
            }

            return true;
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random rnd = new Random();
            StringBuilder sb = new StringBuilder();
            char cr = new char();
            StringBuilder AddLetter (StringBuilder sb)
            {
                for (int i = 0; i < 4; i++)
                {
                    cr = (char)rnd.Next(65, 90);
                    sb.Append(cr);
                }

                return sb;
            }

            StringBuilder AddDigit(StringBuilder sb)
            {
                for (int i = 0; i < 2; i++)
                {
                    cr = (char)rnd.Next(48, 57);
                    sb.Append(cr);
                }
                return sb;
            }

            sb = AddDigit(AddLetter(AddDigit(sb).Append('-')).Append('-'));
            return sb.ToString();
        }
    }
}

