﻿using System;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public const decimal InitialParkingBalance = 0;
        public const int ParkingCapacity = 10;
        public const int PaymentWriteOffPeriod = 5;
        public const int LogPeriod = 60;
        public const int PassengerCarTariffs = 2;
        public const int TruckTariffs = 5;
        public const double BusTariffs = 3.5;
        public const int MotorcycleTariffs = 1;
        public const double PenaltyFactor = 2.5;
    }
}

