﻿using System;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Collections.ObjectModel;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        readonly Parking parking = Parking.GetInstance();
        public ITimerService WTimerService { get; set; }
        public ITimerService LogTimer { get; set; }
        public ILogService LogService { get; set; }
       

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            WTimerService = withdrawTimer;
            LogTimer = logTimer;
            LogService = logService;
        }
        public void AddVehicle(Vehicle vehicle)
        {
            if (parking.ParkVehicles.Count <= 10 )
            {
                parking.ParkVehicles.Add(vehicle);
            }
            else
            {
                throw new InvalidOperationException("Parking is full");
            }
        }

        public void Dispose()
        {
            LogTimer.Dispose();
        }

        public decimal GetBalance()
        {
            return parking.parkBalance;
        }

        public int GetCapacity()
        {
            int capacity = new();
            capacity = parking.ParkVehicles.Count;
            return capacity;
        }

        public int GetFreePlaces()
        {
            if (parking.ParkVehicles.Count >= 10 )
            {
                throw new InvalidOperationException("Parking is full");
            }

            return 10 - parking.ParkVehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return Array.Empty<TransactionInfo>();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
                return new ReadOnlyCollection<Vehicle>(parking.ParkVehicles);
        }

        public string ReadFromLog()
        {
            LogService = new LogService(LogService.LogPath);
            return LogService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (parking.ParkVehicles.Count == 0)
            {
                throw new InvalidOperationException("Parking is empty");
            }

            foreach (Vehicle vehicle in parking.ParkVehicles)
            {
                if (vehicle.Id == vehicleId && vehicle.Balance >= 0)
                {
                    parking.ParkVehicles.Remove(vehicle);
                }
                else if (vehicle.Balance < 0)
                {
                    throw new InvalidOperationException("Inadequate Vehicle Balance");
                }
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            foreach (Vehicle vehicle in parking.ParkVehicles)
            {
                if (vehicle.Id == vehicleId)
                {
                    vehicle.Balance+=sum;
                }
            }
        }
    }
}
