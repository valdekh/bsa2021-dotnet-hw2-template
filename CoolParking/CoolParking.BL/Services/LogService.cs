﻿using System;
using System.IO;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; set; }

        public LogService(string logPath)
        {
            LogPath = logPath;
        }

        public string Read()
        {
            try
            {
                using (StreamReader sr = new StreamReader(LogPath))
                {
                    return sr.ReadToEnd();
                }
            }

            catch
            {
                throw new InvalidOperationException("file is not found");
            }
        }

        public void Write(string logInfo)
        {
            using (StreamWriter sw = new StreamWriter(LogPath,true))
            {
                sw.WriteLine(logInfo);
            }
        }
    }
}

